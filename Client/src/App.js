// import logo from './logo.svg';
// import './App.css';
import MyNavbar from './Navbar';
import Create from './Create';
import Edit from './Edit';
import Search from './Search'


function App() {
  return (
    <>
      <MyNavbar />
      <Create name="Player"/>
      <Edit name="Player"/>
      <Search name="Player"/>
    </>
  );
}

export default App;
