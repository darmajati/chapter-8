import { useRef, useState } from "react";
import { Button, Container, Form, Card } from "react-bootstrap";

export default function Edit(props) {
  const {name} = props
  const usernameInput = useRef(null);
  const emailInput = useRef(null);
  const passwordInput = useRef(null);
  const newUsernameInput = useRef(null);
  const newEmailInput = useRef(null);
  const newPasswordInput = useRef(null);

  const [username, setUsername] = useState();
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [newUsername, setNewUsername] = useState();
  const [newEmail, setNewEmail] = useState();
  const [newPassword, setNewPassword] = useState();

  const handleSubmit = (e) => {
    e.preventDefault();

    setUsername(usernameInput.current.value);
    setEmail(emailInput.current.value);
    setPassword(passwordInput.current.value);
    setNewUsername(newUsernameInput.current.value);
    setNewEmail(newEmailInput.current.value);
    setNewPassword(newPasswordInput.current.value);
  };

  return (
    <>
      <Container className="mt-5">
        <h1 className="mt-5 mb-4" >Edit {name}</h1>
        <h5 className="mb-2">Current {name} Data</h5>
        <Form onSubmit={handleSubmit}>
          <Form.Group className="mb-3" controlId="formBasicUsername">
            <Form.Label>Enter Current Username</Form.Label>
            <Form.Control ref={usernameInput} type="text" placeholder="Current Username" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Enter Current Email address</Form.Label>
            <Form.Control ref={emailInput} type="email" placeholder="Current Email address" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Enter Current Password</Form.Label>
            <Form.Control ref={passwordInput} type="password" placeholder="Current Password" />
          </Form.Group>

          <h5 className=" mt-5 mb-2">New {name} Data</h5>

          <Form.Group className="mb-3" controlId="formBasicUsername">
            <Form.Label>Enter New Username</Form.Label>
            <Form.Control ref={newUsernameInput} type="text" placeholder="New Username" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Enter New Email address</Form.Label>
            <Form.Control ref={newEmailInput} type="email" placeholder="New Email address" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Enter New Password</Form.Label>
            <Form.Control ref={newPasswordInput} type="password" placeholder="New Password" />
          </Form.Group>

          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>

        <Card className="mt-5">
          <Card.Body>
            Current Username: {username} <br />
            Current Email: {email} <br />
            Current Password: {password} <br />
            New Username: {newUsername} <br />
            New Email: {newEmail} <br />
            New Password: {newPassword}
          </Card.Body>
        </Card>
      </Container>
    </>
  );
}
