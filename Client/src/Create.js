import { useRef, useState } from "react";
import { Button, Container, Form, Card} from "react-bootstrap";


export default function Create(props){
  const {name} = props
  const usernameInput = useRef(null);
  const emailInput = useRef(null);
  const passwordInput = useRef(null);

  const [username, setUsername] = useState();
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();

  const handleSubmit = (e) => {
    e.preventDefault();

    setUsername(usernameInput.current.value);
    setEmail(emailInput.current.value);
    setPassword(passwordInput.current.value);
  }

  return(
    <>
      <Container className="mt-5">
        <h1>Create New {name}:</h1>
        <Form onSubmit={handleSubmit}>
          <Form.Group className="mb-3" controlId="formBasicUsername">
            <Form.Label>Username</Form.Label>
            <Form.Control ref={usernameInput} type="text" placeholder="Enter Username" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control ref={emailInput} type="email" placeholder="Enter email" />
            <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control ref={passwordInput} type="password" placeholder="Password" />
          </Form.Group>

          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>

        <Card className="mt-5">
          <Card.Body>
            Username: {username} <br />
            Email: {email} <br />
            Password: {password}
          </Card.Body>
        </Card>
        </Container>
    </>
    );
}