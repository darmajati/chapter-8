import { useRef, useState } from "react";
import { Button, Container, Form, Card} from "react-bootstrap";


export default function Edit(props){
  const {name} = props
  const usernameInput = useRef(null);
  const emailInput = useRef(null);
  const experienceInput = useRef(null);
  const levelInput = useRef(null);

  const [username, setUsername] = useState();
  const [email, setEmail] = useState();
  const [experience, setExperience] = useState();
  const [level, setLevel] = useState();

  const handleSubmit = (e) => {
    e.preventDefault();

    setUsername(usernameInput.current.value);
    setEmail(emailInput.current.value);
    setExperience(experienceInput.current.value);
    setLevel(levelInput.current.value);
  }

  return(
    <>
      <Container className="mt-5">
      <h1 className="mt-5">Search {name}</h1>
        <Form onSubmit={handleSubmit}>
          <Form.Group className="mb-3" controlId="formBasicUsername">
            <Form.Label>Search By Username</Form.Label>
            <Form.Control ref={usernameInput} type="text" placeholder="Username" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Search By Email </Form.Label>
            <Form.Control ref={emailInput} type="email" placeholder="Email" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Search By Experience</Form.Label>
            <Form.Control ref={experienceInput} type="text" placeholder="Experience" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Search By Level</Form.Label>
            <Form.Control ref={levelInput} type="text" placeholder="Level" />
          </Form.Group>

          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>

        <Card className="mt-5">
          <Card.Body>
            Username: {username} <br />
            Email: {email} <br />
            Experience: {experience} <br />
            Level: {experience}
          </Card.Body>
        </Card>
        </Container>
    </>
    );
}